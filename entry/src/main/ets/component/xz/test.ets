/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import {
  File,
  OutputStream,
  InputStream,
  IOUtils,
  CompressorStreamFactory,
  CompressorOutputStream,
  CompressorInputStream
} from '@ohos/commons-compress';
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
export struct XZTest {
  @State isCompressBzip2FileShow: boolean = false;
  @State isDeCompressBzip2Show: boolean = false;
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('XZ相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成hello.txt')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressBzip2FileShow) {
        Text('点击压缩hello.txt为XZ文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testXZCreation()
            }
          })
      }


      if (this.isDeCompressBzip2Show) {
        Text('点击解压XZ文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.unXZFileTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      const writer = fileio.openSync(data + '/hello.txt', 0o102, 0o666);
      fileio.writeSync(writer, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
      );
      fileio.closeSync(writer);
      AlertDialog.show({ title: '生成成功',
        message: '请查看沙箱路径' + data + '/hello.txt',
        confirm: { value: 'OK', action: () => {
          this.isCompressBzip2FileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  testXZCreation(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      console.info('directory obtained. Data:' + data);
      let input = new File(data as string, "/hello.txt");
      let output = new File(data as string, "/hello.txt.xz");
      let out: OutputStream = new OutputStream();
      let input1: InputStream = new InputStream();
      out.setFilePath(output.getPath());
      input1.setFilePath(input.getPath());
      let cos: CompressorOutputStream = new CompressorStreamFactory(false).createCompressorOutputStream("xz", out)
      IOUtils.copyStream(input1, cos);
      cos.close();
      input1.close()
      AlertDialog.show({ title: '压缩成功',
        message: '请查看沙箱路径 ' + data + '/hello.txt.xz',
        confirm: { value: 'OK', action: () => {
          this.isDeCompressBzip2Show = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  unXZFileTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      let input = new File(data as string, "/hello.txt.xz");
      let output = new File(data as string, "/hello1.txt");
      let out: OutputStream = new OutputStream();
      let input1: InputStream = new InputStream();
      out.setFilePath(output.getPath());
      input1.setFilePath(input.getPath());
      let inputs: CompressorInputStream = new CompressorStreamFactory(false).createCompressorNameInputStream("xz", input1)
      IOUtils.copyStream(inputs, out);
      out.close();
      input1.close();
      AlertDialog.show({ title: '解缩成功',
        message: '请查看沙箱路径 ' + data + '/hello1.txt',
        confirm: { value: 'OK', action: () => {
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}






